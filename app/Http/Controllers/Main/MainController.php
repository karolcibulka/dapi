<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Model\Main\History;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MainController extends Controller
{
    public function index(){
        $records = DB::table('histories as h')
            ->select('*','h.created_at as created')
            ->join('assignments as a','h.assignment_id','=','a.id')
            ->join('solutions as s','h.solution_id','=','s.id')
            ->orderBy('created','desc')
            ->paginate(20);

        return view('welcome')->with(array('records'=>$records));
    }
}
