<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Cron\CronController;
use App\Model\Api\Assignment;
use App\Model\Api\Solution;
use App\Model\Api\Stack;
use App\Model\Main\History;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ApiController extends Controller
{

    public $extensions = array();
    public $assignment = null;
    public $solution = null;

    public function __construct(){
        $this->extensions = ['c','php','py'];
    }

//    public function saveRequest(Request $request){
//
//        $validator = Validator::make($request->all(), [
//            'file_input' => 'required|mimes:txt',
//            'moodle_course_id' => 'required',
//            'moodle_assignment_id' => 'required'
//        ]);
//
//        if($validator->fails()){
//            return response()->json( array( 'status' => 0 , 'message' => $validator->errors() ),422);
//        }
//
//        $data = $request->all();
//
//        $opened_file = file_get_contents( $data['file_input'] ,true);
//        $parsed_file = $this->parseTxtToArray( $opened_file , $data['moodle_assignment_id'] , $data['moodle_course_id'] );
//
//        if( !empty( $parsed_file ) ){
//
//            $save_assignment = Assignment::insertGetId( $parsed_file );
//
//            if( !is_null( $save_assignment ) ){
//
//                return response()->json( array( 'status' => 1 , 'message' => 'assignment added successfully' ),200);
//
//            }
//            else{
//
//                return response()->json( array( 'status' => 0 , 'message' => 'assignment added unsuccessfully' ),422);
//
//            }
//
//        }
//
//        return response()->json( array( 'status' => 0 , 'message' => 'unparsed file' ),422);
//    }

    public function saveSolution(Request $request){

        $validator = Validator::make($request->all(), [
            'file_name' => 'required',
            'content' => 'required',
            'server_path' => 'required',
            'moodle_course_id' => 'required',
            'assign' => 'required',
            'student_id' => 'required',
            'moodle_assignment_id' => 'required'
        ]);

        if($validator->fails()){
            return response()->json( array( 'status' => 0 , 'message' => $validator->errors() ),422);
        }

        $data = $request->all();


        $assign_id = $this->getAssignIdOrCreateNewAssign($data['moodle_course_id'],$data['moodle_assignment_id'],$data['assign']);

        $file = $this->createNewFile( $data['file_name'] , $data['content'] );


        if(!is_null($file) && !empty($file)){
            $array = array(
                'extension' => $file['extension'],
                'path_to_file' => $file['name'],
                'created_at' => date('Y-m-d H:i:s'),
                'server_path' => $data['server_path'],
                'student_id' => $data['student_id'],
                'moodle_course_id' => $data['moodle_course_id'],
                'moodle_assignment_id' => $data['moodle_assignment_id'],
                'in' => $data['content'],
                'solved' => '0',
            );

            $solution_id = $this->checkIfSolutionExist( $data['moodle_course_id'] , $data['moodle_assignment_id'] , $data['student_id'] , $array );

            if (!is_null($solution_id)) {

                $array_stack = array(
                    'assignment_id' => $assign_id,
                    'solution_id' => $solution_id,
                    'solved' => '0',
                    'created_at' => date('Y-m-d H:i:s')
                );

                $parsedAssign = $this->parseTxtToArray( $data['assign'] );

                $response = array();
                $response['output_solution'] = $this->doJob( $array_stack );
                $response['solution'] = $data['content'];
                $response['output_assign'] = isset($parsedAssign['output']) && !empty($parsedAssign['output']) ? $parsedAssign['output'] : '';

                if($response['output_solution'] == $response['output_assign']){
                    return response()->json(array('solved' => 1, 'status' => 1, 'response' => $response , 'message' => 'solution added successfully'), 200);
                }
                else{
                    return response()->json(array('solved' => 0, 'status' => 1, 'response' => $response , 'message' => 'solution added successfully'), 200);
                }
            }
            else {
                return response()->json( array( 'status' => 0 , 'message' => 'solution added unsuccessfully' ),422);
            }
        }
        return response()->json( array( 'status' => 0 , 'message' => 'Wrong extension' ),422);
    }

    private function checkStackAndCreate($array_stack){
        $stack = Stack::where('assignment_id','=',$array_stack['assignment_id'])
            ->where('solution_id','=',$array_stack['solution_id'])
            ->where('solved','=','0')
            ->get()
            ->first();

        if(is_null($stack)){
            Stack::insert($array_stack);
        }

        return true;

    }

    private function checkIfSolutionExist($course_id,$assign_id,$student_id,$array){
        $solution = Solution::where('moodle_course_id','=',$course_id)
            ->where('moodle_assignment_id','=',$assign_id)
            ->where('student_id','=',$student_id)
            ->get()
            ->first();
        if(!is_null($solution)){
            $solution = $solution->toArray();

            $old_file_path = 'solutions/'.$this->getExtension($solution['path_to_file']).'/'.$solution['path_to_file'];
            if( file_exists( $old_file_path ) ){
                unlink( $old_file_path );
            }

            $update_array = array(
                'updated_at' => date('Y-m-d H:i:s'),
                'in' => $array['in'],
                'path_to_file' => $array['path_to_file'],
                'solved' => '0',
                'extension' => $array['extension']
            );

            Solution::where('id','=',$solution['id'])->update($update_array);

            return $solution['id'];
        }
        else{
            $solution_id = Solution::insertGetId($array);
            return $solution_id;
        }
    }

    private function getAssignIdOrCreateNewAssign($course_id,$assign_id,$assign){
        $res = $this->parseTxtToArray($assign,$assign_id,$course_id);
        $id = Assignment::where('moodle_course_id','=',$res['moodle_course_id'])
                        ->where('moodle_assignment_id','=',$res['moodle_assignment_id'])
                        ->get()
                        ->first();
        if(!is_null($id)){
            return $id['id'];
        }

        $id = Assignment::insertGetId($res);
        return $id;

    }

    private function createNewFile( $file_name , $content ){
        $extension = $this->getExtension( $file_name );

        $response = null;

        if(in_array($extension,$this->extensions)){
            $name = uniqid() . '_'. time() . '.' . $extension ;

            $destination_path = 'solutions/' . $extension . '/';
            $file = fopen($destination_path.$name, "w") or die("Unable to open file!");
            fwrite( $file , $content );
            fclose( $file );

            $response = array(
                'extension' => $extension,
                'name' => $name,
            );
        }

        return $response;
    }
    private function getExtension($name){
        $exploded = explode('.',$name);
        return $exploded['1'];
    }

    private function parseTxtToArray($file , $assignment_id=null , $course_id=null ){

        $exploded = explode("\n",$file);
        $output = array(
            'inputs' => null,
            'output' => null,
            'language' => null,
            'name' => null,
            'moodle_assignment_id' => $assignment_id,
            'moodle_course_id' => $course_id,
            'created_at' => date('Y-m-d H:i:s')
        );

        $input_row =  null;
        $output_row = null;
        $inputs = array();

        if(!empty($exploded)){
            foreach($exploded as $key => $row){
                if($key == 0){
                    $explode_first_row = explode(' ',$row);
                    $output['name'] = implode(' ',$explode_first_row);
                    $output['language'] = strtolower($explode_first_row[array_key_last($explode_first_row)]);
                }
                if($row == 'VSTUP'){
                    $input_row = $key;
                }
                if($key > $input_row && $exploded[$key]!=='VYSTUP' && is_null($output_row)){
                    $inputs[] = $row;
                }
                if($row == 'VYSTUP'){
                    $output_row = $key;
                }
                if(!is_null($output_row) && $key>$output_row){
                    $output['output'] = $row;
                }
                continue;
            }
            if(!empty($inputs)){
                $output['inputs'] = serialize($inputs);
            }
        }

        return $output;
    }

    public function doJob($array_stack){

        if( isset( $array_stack ) && !empty( $array_stack) ){

            $assignment = Assignment::where('id','=',$array_stack['assignment_id'])->get()->first();
            $solution = Solution::where('id','=',$array_stack['solution_id'])->get()->first();

            if( $assignment && $solution ){
                $this->assignment = $assignment->toArray();
                $this->solution = $solution->toArray();

                $response = null;
                switch( $this->solution['extension'] ){
                    case "c":
                        $response = $this->doC();
                        break;
                    case "php":
                        $response = $this->doPhp();
                        break;
                    case "py":
                        $response = $this->doPy();
                        break;
                    default:
                        echo "No method for compile!";
                        exit;
                }

                if( !is_null( $response ) ){

                    return $response;
                }

            }
            else{
                return 'doesnt exist solution or assignment';
            }
        }
        else{
            return 'stack is empty';
        }
    }

    private function doC(){
        $path = public_path().'/solutions/'.$this->solution['extension'].'/';
        $errors =  shell_exec('gcc -o '.$path.'compiled '.$path.$this->solution['path_to_file'].' 2>&1');


        if(!is_null($errors) && strpos($errors,'gcc: error trying to exec') === false){
            $response = $errors;
        }
        else{
            $arguments = '';
            if(!empty($this->assignment['inputs'])){
                $arguments = ' '.$this->doArguments($this->assignment['inputs']);
            }
            $response = shell_exec($path.'compiled'.$arguments);
        }

        Solution::where( 'id' , '=' , $this->solution['id'] )->update(
            array(
                'response' => $response ,
                'in' => file_get_contents($path.$this->solution['path_to_file']),
                'solved' => $this->assignment['output'] == $response ? '1' : '0',
            )
        );


        History::insert(array(
            'assignment_id' => $this->assignment['id'],
            'solution_id' => $this->solution['id'],
            'created_at' => date('Y-m-d H:i:s')
        ));

        return $response;
    }

    private function doPhp(){
        $path = public_path().'/solutions/'.$this->solution['extension'].'/';
        $arguments = ' ';
        if(!empty($this->assignment['inputs'])){
            $arguments .= $this->doArguments($this->assignment['inputs']);
        }

        $response =  shell_exec('php -q '.$path.$this->solution['path_to_file'].$arguments);

        Solution::where( 'id' , '=' , $this->solution['id'] )->update(
            array(
                'response' => $response ,
                'in' => file_get_contents($path.$this->solution['path_to_file']),
                'solved' => $this->assignment['output'] == $response ? '1' : '0',
            )
        );

        History::insert(array(
            'assignment_id' => $this->assignment['id'],
            'solution_id' => $this->solution['id'],
            'created_at' => date('Y-m-d H:i:s')
        ));

        return $response;
    }

    private function checkExistingHistoryItem(){
        $response = History::where('assignment_id','=',$this->assignment['id'])->where('solution_id','=',$this->solution['id'])->get()->first();
        if( !is_null( $response ) ){
            return false;
        }
        return true;
    }

    private function doPy(){

    }


    private function doArgumentsPhp ( $array ){
        $unserializedArray = unserialize( $array );
        $arguments = '';
        if( !empty( $unserializedArray ) ){
            foreach($unserializedArray as $key => $item){
                if($key == '0'){
                    $arguments .= '?value'.$key.'='.$item;
                }
                else{
                    $arguments .= '&value'.$key.'='.$item;
                }
            }
        }

        return $arguments;
    }

    private function doArguments( $array ){
        $unserializedArray = unserialize( $array );
        $arguments = '';
        if( !empty( $unserializedArray ) ){
            $arguments = implode(' ', $unserializedArray );
        }
        return $arguments;
    }
}
