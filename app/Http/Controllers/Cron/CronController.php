<?php

namespace App\Http\Controllers\Cron;

use App\Http\Controllers\Controller;
use App\Model\Api\Assignment;
use App\Model\Api\Solution;
use App\Model\Api\Stack;
use App\Model\Main\History;use Illuminate\Http\Request;

class CronController extends Controller
{

    public $assignment = null;
    public $solution = null;

    public function doJob(){
        //dd($array_stack);exit;
        $job = Stack::where('solved','=','0')->get()->first();
        if( $job ){
            $job = $job->toArray();
            $assignment = Assignment::where('id','=',$job['assignment_id'])->get()->first();
            $solution = Solution::where('id','=',$job['solution_id'])->get()->first();

            if( $assignment && $solution ){
                $this->assignment = $assignment->toArray();
                $this->solution = $solution->toArray();

                $response = null;
                switch( $this->solution['extension'] ){
                    case "c":
                        $response = $this->doC();
                        break;
                    case "php":
                        $response = $this->doPhp();
                        break;
                    case "py":
                        $response = $this->doPy();
                        break;
                    default:
                        echo "No method for compile!";
                        exit;
                }

                if( !is_null( $response ) ){

                    dd($response);
                    //TODO::__this->connectToApiAndSendResponse();
                }

            }
            else{
                echo 'doesnt exist solution or assignment';
                exit;
            }
        }
        else{
            echo 'stack is empty';
            exit;
        }
    }

    private function doC(){
        $path = public_path().'/solutions/'.$this->solution['extension'].'/';
        $errors =  shell_exec('gcc -o '.$path.'compiled '.$path.$this->solution['path_to_file'].' 2>&1');

        if(!is_null($errors)){
            $response = $errors;
        }
        else{
            $arguments = '';
            if(!empty($this->assignment['inputs'])){
                $arguments = ' '.$this->doArguments($this->assignment['inputs']);
            }
            $response = shell_exec($path.'compiled'.$arguments);
        }

        Solution::where( 'id' , '=' , $this->solution['id'] )->update(
            array(
                'response' => $response ,
                'in' => file_get_contents($path.$this->solution['path_to_file']),
                'solved' => $this->assignment['output'] == $response ? '1' : '0',
            )
        );

        History::insert(array(
            'assignment_id' => $this->assignment['id'],
            'solution_id' => $this->solution['id'],
            'created_at' => date('Y-m-d H:i:s')
        ));


        return $response;
    }

    private function doPhp(){
        $path = public_path().'/solutions/'.$this->solution['extension'].'/';
        $arguments = ' ';
        if(!empty($this->assignment['inputs'])){
            $arguments .= $this->doArguments($this->assignment['inputs']);
        }

        $response =  shell_exec('php -q '.$path.$this->solution['path_to_file'].$arguments);

        Solution::where( 'id' , '=' , $this->solution['id'] )->update(
            array(
                'response' => $response ,
                'in' => file_get_contents($path.$this->solution['path_to_file']),
                'solved' => $this->assignment['output'] == $response ? '1' : '0',
            )
        );

        History::insert(array(
            'assignment_id' => $this->assignment['id'],
            'solution_id' => $this->solution['id'],
            'created_at' => date('Y-m-d H:i:s')
        ));


        return $response;
    }

    private function checkExistingHistoryItem(){
        $response = History::where('assignment_id','=',$this->assignment['id'])->where('solution_id','=',$this->solution['id'])->get()->first();
        if( !is_null( $response ) ){
            return false;
        }
        return true;
    }

    private function doPy(){

    }


    private function doArgumentsPhp ( $array ){
        $unserializedArray = unserialize( $array );
        $arguments = '';
        if( !empty( $unserializedArray ) ){
            foreach($unserializedArray as $key => $item){
                if($key == '0'){
                    $arguments .= '?value'.$key.'='.$item;
                }
                else{
                    $arguments .= '&value'.$key.'='.$item;
                }
            }
        }

        return $arguments;
    }

    private function doArguments( $array ){
        $unserializedArray = unserialize( $array );
        $arguments = '';
        if( !empty( $unserializedArray ) ){
            $arguments = implode(' ', $unserializedArray );
        }
        return $arguments;
    }
}
