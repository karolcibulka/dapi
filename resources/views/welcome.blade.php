<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous"><title>Stack history</title>
</head>
<body>
<style>
    .mt-5{
        margin-top:50px;
    }
    .showInRes{
        cursor:pointer;
        color:darkred;
        font-weight: bold;
    }
</style>

<div id="footer" class="text-center" style="position:aboslute;top:0;left:0;width:100%;height:40px;">
    <span>
        Karol Cibulka &copy; <b>{{date('Y')}}</b>
    </span>
</div>

<div class="container mt-5">
    <div class="row">
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>#MC_ID</th>
                    <th>#MA_ID</th>
                    <th>#S_ID</th>
                    <th>NAME</th>
                    <th>LANG</th>
                    <th>Last edit</th>
                    <th>IN - RES</th>
                    <th>SOLVED</th>
                </tr>
                </thead>
                <tbody>
                    @if(!empty($records))
                        @foreach($records as $record)
                            <tr>
                                <td>{{$record->moodle_course_id}}</td>
                                <td>{{$record->moodle_assignment_id}}</td>
                                <td>{{$record->student_id}}</td>
                                <td>{{$record->name}}</td>
                                <td>{{$record->extension}}</td>
                                <td>{{$record->created}}</td>
                                <td><span class="showInRes" data-assignment_id="{{$record->assignment_id}}" data-solution_id="{{$record->solution_id}}" data-course_id="{{$record->moodle_course_id}}" data-in="{{json_encode($record->in,TRUE)}}" data-res="{{json_encode($record->response)}}">show IN-RES</span></td>
                                <td>{{$record->solved == '1' ? 'TRUE' : 'FALSE'}}</td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>

            {{$records->links()}}
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document" style="min-width:80% !important">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><span class="modal_id"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <h5 class="text-center">VSTUP</h5>
                        <pre id="in">
asd
                        </pre>
                    </div>
                    <div class="col-md-6">
                        <h5 class="text-center">VÝSTUP</h5>
                        <pre id="out">
asdasd
                        </pre>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script
    src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
    crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
<script>
    $('.showInRes').on('click',function(){
        var INNER = JSON.parse($(this).data().in);
        var inner = INNER.replace("?php",'___uniquestringMakedByMe');
        inner = inner.replace("<___uniquestringMakedByMe",'');
        console.log(inner);
        $('#exampleModal').find('.modal_id').html($(this).data().course_id +"/"+$(this).data().assignment_id+"/"+$(this).data().solution_id);
        $('#exampleModal').find('#in').html(inner);
        $('#exampleModal').find('#out').html(JSON.parse($(this).data().res));
        $('#exampleModal').modal();
    });
</script>

</body>
</html>
